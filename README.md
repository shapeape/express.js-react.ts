# Express.js + React TS

Create a simple Express.js Backend + a React Typescript Frontend using yarn Workspaces.  
You might need a setup like this to resolve CORS issues.

Following this [Video](https://youtu.be/w3vs4a03y3I) i used **TypeScript** on the Frontend side and **yarn** instead of npm.  
Also i added [Workspaces](https://yarnpkg.com/features/workspaces#gatsby-focus-wrapper), as described in this [Video](https://youtu.be/ACDGXHR_YmI)

This is a fast forward walk-through:

```bash
mkdir project-name
yarn init
```

- name: project-name
- version: 1.0.0
- description: express-server with react ts frontend.
- entry point:
- repository-url:
- author: your-name
- license: MIT
- private: yes

```bash
mkdir project-name/server
cd project-name/server
yarn init
```

- name: server
- version: 1.0.0
- description: express-server
- entry point: server.js
- repository-url:
- author: your-name
- license: MIT
- private: yes

Create react-app with typescript flavour:

```bash
cd ..
yarn create react-app client --template typescript
```

Add Workspaces (edit the main package.json):

```bash
nano package.json
```

Add:

```json
  "private": true,
  "workspaces": [
    "server",
    "client"
  ]
```

The names of the workspaces must match the names in their own package.json files (client/package.json, server/package.json).

Add dependencies for server:

```bash
yarn workspaces server add express
yarn workspaces server add nodemon --dev
```

Add a server.js:

```bash
cd ../server
touch server.js
```

Add scripts to start and auto-restart the server:

```bash
nano package.json
```

add:

```json
"scripts": {
    "start": "node server",
    "dev": "nodemon server"
},
```

Edit server.js:

```bash
nano server.js
```

```javascript
const   express = require("express");
const   app = express();

app.get("/api", (req, res) => {
  res.json({
    users: \["userOne", "userTwo", "useThree"\],
  });
});



app.listen(5000, () => {
  console.log("Server started on port 5000");
});
```

Start the server:

```bash
yarn run dev
```

Add a proxy to the client:

```bash
cd ../client
nano package.json
```

Add:

```json
"proxy": "http://localhost:5000",
```

    nano src/App.tsx

```typescript
import React, { useEffect, useState } from "react";

function App() {
  const [backendData, setBackendData] = useState<Record<string, string[]>>();

  useEffect(() => {
    fetch("/api")
      .then((response) => response.json())
      .then((data) => {
        setBackendData(data);
      });
  }, []);

  return (
    <div>
      {typeof backendData?.users === "undefined" ? (
        <p>Loading...</p>
      ) : (
        backendData.users.map((user, i) => <p key={i}>{user}</p>)
      )}
    </div>
  );
}

export default App;
```

Run both client and server, each in their own terminal:

    yarn workspace server start
    yarn workspace client start

To run both using a single script, we install concurrently.  
In the main folder:

```bash
yarn add concurrently -D -W
```

in the main package.json we add as script:

```json
"scripts": {
    "start": "concurrently \"yarn workspace server start\" \"yarn workspace client start\""
  },
```

Now we can start client and server with a single command:

```bash
yarn start
```
